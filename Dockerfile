FROM --platform=${TARGETPLATFORM} ghcr.io/linuxserver/baseimage-ubuntu:noble

# set version label
ARG BUILD_DATE
ARG VERSION
ARG CODE_RELEASE

# environment settings
ARG DEBIAN_FRONTEND="noninteractive"
ENV HOME="/config"

## environment settings added
ENV TZ=Europe/Paris

## when building for arm64 on gitlab, error:
##   debconf: apt-extracttemplates failed: No such file or directory
##   Error while loading /usr/sbin/dpkg-split: No such file or directory
## Seen on https://github.com/docker/buildx/issues/495
## Still:
##  Error template parse error: loading: No such file or directory at /usr/share/perl5/Debconf/Template.pm
## But don't know how to avoid and seems not fatal
## OK with Docker Desktop on Mac Intel
# RUN ln -s /usr/bin/dpkg-split /usr/sbin/dpkg-split && \
#     ln -s /usr/bin/dpkg-deb /usr/sbin/dpkg-deb && \
#     ln -s /bin/rm /usr/sbin/rm && \
#     ln -s /bin/tar /usr/sbin/tar

## From Dockerfile@linuxserver
RUN                                                \
     echo "**** install linuxserver packages ****" \
  && apt-get update                                \
  && apt-get install -y --no-install-recommends    \
       git                                         \
       libatomic1                                  \
       nano                                        \
       net-tools                                   \
       sudo

## Additions
RUN                                                \
     echo "**** install added packages ****"       \
  && apt-get update                                \
  && apt-get install -y --no-install-recommends    \
       curl                                        \
       file                                        \
       procps                                      \
       python3                                     \
       python3-pip                                 \
       unzip                                       \
       zip                                         \
       zsh                                         \
  ## pip is pip3, but python not set
  && ln -s /usr/bin/python3 /usr/bin/python

ARG CODE_ARCH
RUN                                                \
     echo "**** install openvscode-server ****"    \
  && if [ -z ${CODE_RELEASE+x} ]; then             \
    CODE_RELEASE=$(curl -sX                        \
        GET "https://api.github.com/repos/gitpod-io/openvscode-server/releases/latest" \
      | awk '/tag_name/{print $4;exit}' FS='[""]'  \
      | sed 's|^openvscode-server-v||');           \
     fi                                            \
  && mkdir -p /app/openvscode-server               \
  && arch=$(uname -m)                              \
  && echo arch=${arch}                             \
  && if [ "${arch}" = "x86_64" ]; then             \
       CODE_ARCH="x64";                            \
     elif [ "${arch}" = "aarch64" ]; then          \
       CODE_ARCH="arm64";                          \
     fi                                            \
  && curl -o                                       \
    /tmp/openvscode-server.tar.gz -L               \
    "https://github.com/gitpod-io/openvscode-server/releases/download/openvscode-server-v${CODE_RELEASE}/openvscode-server-v${CODE_RELEASE}-linux-${CODE_ARCH}.tar.gz" \
  && tar xf                                        \
      /tmp/openvscode-server.tar.gz -C             \
      /app/openvscode-server/ --strip-components=1

RUN \
     echo "**** clean up ****" \
  && apt-get clean             \
  && rm -rf                    \
       /tmp/*                  \
       /var/lib/apt/lists/*    \
       /var/tmp/*



## VSCode extension for Python
# ENV PATH=/usr/bin:/usr/sbin:/bin:/sbin
RUN                                               \
     /app/openvscode-server/bin/openvscode-server \
        --install-extension ms-python.python

## See https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner
RUN                                              \
    /app/openvscode-server/bin/openvscode-server \
        --install-extension formulahendry.code-runner


## set sudo password and change shell
ARG SUDO_PASSWORD=abc
RUN                                                         \
     echo "abc ALL=(ALL:ALL) ALL" >> /etc/sudoers           \
  && echo "${SUDO_PASSWORD}\n${SUDO_PASSWORD}" | passwd abc \
  && chsh --shell /bin/zsh abc

## Install on-my-zsh
## Installation of zsh-autosuggestions and zsh-syntax-highlighting via apt fails due to expired GPG keys
RUN                                                                                                            \
     sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"           \
  && sed -i 's/plugins=(.*/plugins=(git vscode zsh-autosuggestions zsh-syntax-highlighting)/' /config/.zshrc   \
  && echo 'PS1="%B%F{blue}%~:%f%b "'                                                        >>/config/.zshrc   \
  && git clone https://github.com/zsh-users/zsh-autosuggestions /config/.oh-my-zsh/custom/plugins/zsh-autosuggestions \
  && git clone https://github.com/zsh-users/zsh-syntax-highlighting /config/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting


## /config/workspace is the initial workspace for VSCode
## /config will be a docker volume, initially empty
## wrapper_script will do the copy from /init-config
## but /config must exist for the mount
RUN                                     \
     mkdir -p /config/workspace/.vscode \
  && mv /config /init-config            \
  && mkdir /config

COPY settings.json /init-config/workspace/.vscode/

## Wrapper script
COPY /root /

## launch
ENTRYPOINT ["/bin/bash", "/usr/local/lib/wrapper_script.sh"]

## ports and volumes
EXPOSE 3000
