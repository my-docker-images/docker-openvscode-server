
# docker-openvscode-server

## Why

This project has been cloned from [marcadetd/docker-openvscode-server](https://github.com/marcadetd/docker-openvscode-server) 
which is itself a fork of [linuxserver/docker-openvscode-server](https://github.com/linuxserver/docker-openvscode-server).

The purpose of this is to be able to adapt this image to my needs and produce images in 
gitlab-research container registry while still being able to apply changes coming from the 
initial project.

When such changes happen:
- create a PR on [marcadetd/docker-openvscode-server](https://github.com/marcadetd/docker-openvscode-server)
- get the corresponding patch file by appending `.patch` to the PR URL (see [here](https://coderwall.com/p/6aw72a/creating-patch-from-github-pull-request))
- apply the PR on [marcadetd/docker-openvscode-server](https://github.com/marcadetd/docker-openvscode-server) 
and the corresponding patch to this repository (`git am --reject -i PR.patch`)

## Changes

- Install preffered and/or needed tools for images which will be based on this one
  - curl
  - nano
  - python3
  - unzip and zip
  - zsh and "Oh My ZSH!"

## Details

- The exposed port is 3000
- The user folder is `/config`
- The user and sudo password is `abc`
- If docker (or podman) is installed on your computer, you can run (`amd64` or `arm64` architecture) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server:latest`
- There is also an image using [Homebrew](https://brew.sh) as package manager to get the 
  latest versions of the tools (tag `brew`, branch `brew`, only `amd64`)

