#!/bin/bash

if [ -z $2 ]; then
    CONNECTION_TOKEN="--without-connection-token"
else
    # <password> is ${2}
    # Add /?tkn=<password> at the end of the URL
    CONNECTION_TOKEN="--connection-token ${2}"
fi

echo "$(hostname -I | cut -f2 -d' ') $HOSTNAME" >> /etc/hosts

cp    -TRn /init-config/ /config
chown -R   abc:abc       /config

exec \
    s6-setuidgid abc \
        /app/openvscode-server/bin/openvscode-server \
            --host 0.0.0.0 \
            --port 3000 \
            --disable-telemetry \
            --default-workspace /config/workspace \
            --default-folder /config/workspace \
            ${CONNECTION_TOKEN}
